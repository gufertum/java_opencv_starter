## OpenCV for IntelliJ/Java/OSX

Short project and instructions to build an run OpenCV on OSX for Java and set it up on IntelliJ.
You can detect faces on pictures and also use your webcam to grab (and detect) some pictures.

Inspired by: https://elbauldelprogramador.com/en/compile-opencv-3.2-with-java-intellij-idea/

### Build (not used anymore, we use org.openpnp libs via maven)

To get the .jar and the native libraries, you have to compile opencv.
Precompiled libs are available in this projects, so you only have to do this if you have
use another platform (not OSX) or if you want a newer version.

Check dependencies as stated (java, python etc...) and install them using 'brew' in case you need it.

- Java 1.8 installed and JAVA_HOME set
- Python 2.6+ installed
- opencv sources downloaded and extracted

Then open a terminal:

- mkdir build
- cd build
- cmake --DBUILD_SHARED_LIBS=OFF ..
- make -j8

Copy the libs (jar and native) to the desired location (optional).

- mkdir -p YOUR_PROJECT/lib/native/osx
- cp .build/bin/opencv-330.jar YOUR_PROJECT/lib
- cp .build/lib/* YOUR_PROJECT/lib/native/osx  

#### Alternative build using maven

There is a maven repo which contains the libs and the native libraries for various platforms.
You can also use that and do not have to compile the libs for yourself.

https://mvnrepository.com/artifact/org.openpnp/opencv/3.2.0-1

```
        <!-- https://mvnrepository.com/artifact/org.openpnp/opencv -->
        <dependency>
            <groupId>org.openpnp</groupId>
            <artifactId>opencv</artifactId>
            <version>3.2.0-1</version>
        </dependency>
``

But you have to live with an 'older' version of opencv.

### Run

After that, just run the Main and check the files generated in the 'output' directory.
The number in the files indicates the number of detected faces.