package li.lambda.opencv.demo;

import org.opencv.core.Core;

public class MainDemo {

    public static void main(String[] args) {

        System.out.print("Hello OpenCV - loading libs ... ");

        //load libs
        nu.pattern.OpenCV.loadShared();

        // Load the native library.
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        System.out.print(" loaded.\n");

        Webcam.grab();

        new DetectFaceDemo().run();
    }
}