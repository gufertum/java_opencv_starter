package li.lambda.opencv.demo;

import org.opencv.core.Core;
import org.opencv.core.Mat;

import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

public class Webcam {

    public static void grab () {

        VideoCapture camera = new VideoCapture(0);
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        camera.open(0); //Useless
        if(!camera.isOpened()){
            System.out.println("Camera Error");
        }
        else{
            System.out.println("Camera OK?");
        }

        Mat frame = new Mat();
        camera.read(frame);
        System.out.println("Frame Obtained");

        // if the frame is not empty, process it
        if (!frame.empty()) {
            Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2GRAY);
        }

        camera.release();

        System.out.println("Captured Frame Width " + frame.width());

        Imgcodecs.imwrite("src/main/resources/images/camera.jpg", frame);
        System.out.println("OK");
    }

    public static void init () {

        System.out.print("Webcam - loading libs ... ");

        //load libs
        nu.pattern.OpenCV.loadShared();

        // Load the native library.
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        System.out.print(" loaded.\n");
    }

    public static void main (String args[]) {

        System.out.println("Hello, OpenCV Webcam");
        // init lib
        Webcam.init();
        // grab an image
        Webcam.grab();
    }
}