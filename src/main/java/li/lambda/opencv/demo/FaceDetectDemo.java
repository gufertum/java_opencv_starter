package li.lambda.opencv.demo;

import org.opencv.core.*;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.objdetect.CascadeClassifier;

import java.util.ArrayList;
import java.util.List;

import static org.opencv.imgproc.Imgproc.rectangle;

//
// Detects faces in an image, draws boxes around them, and writes the results
// to "faceDetection.png".
//
class DetectFaceDemo {

    private final String classfierResource = "lbpcascade_frontalface.xml";

    private final List<String> inputImages = new ArrayList<>();

    private final CascadeClassifier faceDetector = new CascadeClassifier(getClass().getResource("/" + classfierResource).getPath());


    public void run() {

        inputImages.add("camera.jpg");
        inputImages.add("IMG_20170813_144122_0021.JPG");
        inputImages.add("IMG_20171006_210903_0274.JPG");
        inputImages.add("lena.png");
        inputImages.add("AverageMaleFace.jpg");
        inputImages.add("graffiti.png");

        System.out.println("\nRunning DetectFaceDemo...");

        inputImages.forEach( inputImage -> {

            // Create a face detector from the cascade file in the resources
            // directory.
            Mat image = Imgcodecs.imread(getClass().getResource("/images/" + inputImage).getPath());
            int numFacesDetected = detectFaces(image);

            // Save the visualized detection.
            String filename = "output/detected-" + numFacesDetected + "-" + inputImage;
            System.out.println(String.format("  Writing %s", filename));
            Imgcodecs.imwrite(filename, image);

        });
    }

    private int detectFaces(Mat image) {

        // Detect faces in the image.
        // MatOfRect is a special container class for Rect.
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(image, faceDetections);

        int numFacesDetected = faceDetections.toArray().length;

        System.out.println(String.format("Detected %s faces", numFacesDetected));

        // Draw a bounding box around each face.
        for (Rect rect : faceDetections.toArray()) {
            rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
        }
        return numFacesDetected;
    }
}
