package li.lambda.opencv.detectors;

import org.opencv.core.Mat;

public interface DetectorInterface {

    public static final String CASCADE_PATH = "/cascade/";

    @Override
    public String toString();

    public String getDescription();

    /**
     * detect and mark the image and return the info
     * @param image
     * @return info containing the image and the count
     */
    public Info mark(Mat image);

}
