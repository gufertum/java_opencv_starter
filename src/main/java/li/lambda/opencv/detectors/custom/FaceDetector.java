package li.lambda.opencv.detectors.custom;

import li.lambda.opencv.detectors.DetectorInterface;
import li.lambda.opencv.detectors.Info;
import org.opencv.core.*;
import org.opencv.objdetect.CascadeClassifier;

import static org.opencv.imgproc.Imgproc.rectangle;

public class FaceDetector implements DetectorInterface {

    private static final String classfierResource = "lbpcascade_frontalface.xml";

    private static final CascadeClassifier faceDetector = new CascadeClassifier(FaceDetector.class.getClass().getResource(CASCADE_PATH + classfierResource).getPath());
    
    @Override
    public String toString() {
        return "Face detector";
    }

    @Override
    public String getDescription() {
        return "TODO";
    }

    public Info mark(Mat image) {

        // Detect faces in the image.
        // MatOfRect is a special container class for Rect.
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(image, faceDetections);

        //int numFacesDetected = faceDetections.toArray().length;
        //System.out.println(String.format("Detected %s faces", numFacesDetected));

        // Draw a bounding box around each face.
        for (Rect rect : faceDetections.toArray()) {
            rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
        }

        return new Info(image, faceDetections.toArray().length);
    }
}
