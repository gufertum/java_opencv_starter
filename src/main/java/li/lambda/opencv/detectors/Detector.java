package li.lambda.opencv.detectors;

import org.opencv.core.*;
import org.opencv.objdetect.CascadeClassifier;

import static org.opencv.imgproc.Imgproc.rectangle;

public class Detector implements DetectorInterface {

    private String name;
    private String filename;
    private CascadeClassifier faceDetector;

    public Detector(String cascadeFilename) {
        this.filename = cascadeFilename;
        this.faceDetector = new CascadeClassifier(Detector.class.getClass().getResource(CASCADE_PATH + cascadeFilename).getPath());
        this.name = cascadeFilename;
    }

    public Detector(String cascadeFilename, String name) {
        this.filename = cascadeFilename;
        this.faceDetector = new CascadeClassifier(Detector.class.getClass().getResource(CASCADE_PATH + cascadeFilename).getPath());
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public String getDescription() {
        return this.name;
    }

    public Info mark(Mat image) {

        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(image, faceDetections);

        // Draw a bounding box around each detection.
        for (Rect rect : faceDetections.toArray()) {
            rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
        }

        return new Info(image, faceDetections.toArray().length);
    }
}
