package li.lambda.opencv.detectors;

import org.opencv.core.Mat;

public class Info {
    int count = 0; //number of detected objects
    Mat image; //the enriched image

    public Info(Mat image, int count) {
        this.image = image;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public Mat getImage() {
        return image;
    }
}
