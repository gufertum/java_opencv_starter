package li.lambda.opencv.gui;


import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import li.lambda.opencv.detectors.Detector;
import li.lambda.opencv.detectors.DetectorInterface;
import li.lambda.opencv.detectors.Info;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import java.awt.*;
import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The controller for our application, where the application logic is
 * implemented. It handles the button for starting/stopping the camera and the
 * acquired video stream.
 *
 * @author <a href="mailto:luigi.derussis@polito.it">Luigi De Russis</a>
 * @author <a href="http://max-z.de">Maximilian Zuleger</a> (minor fixes)
 * @version 2.0 (2016-09-17)
 * @since 1.0 (2013-10-20)
 */
public class MainController implements Initializable {

    private static final boolean doResize = true;

    // the FXML combobox
    @FXML
    private ComboBox<DetectorInterface> combobox;
    // the FXML button
    @FXML
    private Button button;
    // the FXML label to show the detected object counter
    @FXML
    private Label text;
    // the FXML image view
    @FXML
    private ImageView currentFrame;
    // the FXML label to show the status
    @FXML
    private Label status;
    //root pane
    @FXML
    private BorderPane rootPane;

    private static final DecimalFormat DF = new DecimalFormat("#.00");

    // a timer for acquiring the video stream
    private ScheduledExecutorService timer;
    // the OpenCV object that realizes the video capture
    private VideoCapture capture = new VideoCapture();
    // a flag to change the button behavior
    private boolean cameraActive = false;
    // the id of the camera to be used
    private static int cameraId = 0;
    // the last label to keep track of updates
    private String oldText = "";

    // the currently used marker interface (implementation)
    private volatile DetectorInterface currentMarker;

    // list of all detectors (for the combobox to select)
    private ObservableList<DetectorInterface> detectors;

    // target dimension for images (to display and calculate)
    private Dimension TARGET_DIMENSION = new Dimension(640, 360);

    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {

        //add all detectors
        detectors = FXCollections.observableArrayList();

        URL url = MainController.class.getClassLoader().getResource("./cascade");
        File[] files = new File(url.getPath()).listFiles();
        Arrays.sort(files);
        for (File f: files) {
            if (f.exists() && f.isFile() && !f.isHidden() && f.getName().endsWith(".xml")) {
                detectors.add(new Detector(f.getName()));
            }
        }

        //detectors.add(new Detector("lbpcascade_frontalface.xml");
        //detectors.add(new Detector("lbpcascade_frontalface_improved.xml", "Improved frontal face");
        //detectors.add(new ProfileFaceDetector());

        //set them in the combobox
        combobox.setItems(detectors);
        //on selection, set them...
        combobox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            currentMarker = newValue;
        });
        //preselect the first item
        combobox.getSelectionModel().selectFirst();

        //enable drag and drop for images to detect
        rootPane.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (db.hasFiles()) {
                    event.acceptTransferModes(TransferMode.COPY);
                } else {
                    event.consume();
                }
            }
        });

        rootPane.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasFiles()) {
                    success = true;
                    String filePath = null;
                    for (File file:db.getFiles()) {
                        filePath = file.getAbsolutePath();
                        //System.out.println(filePath);
                        Mat frame = Imgcodecs.imread(filePath);
                        processFrame(frame);
                    }
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });

        //update labels
        updateStatus("Welcome to OpenCV: start the camera or drag an image!");
        updateLabel("");
    }

    /**
     * Processs a captured or loaded frame and display it in the image view.
     * @param frame
     */
    private void processFrame(final Mat frame) {
        Dimension scaled = Utils.getScaledDimension(new Dimension(frame.width(), frame.height()), TARGET_DIMENSION);
        Imgproc.resize(frame, frame, new Size(scaled.width, scaled.height), 0, 0, Imgproc.INTER_CUBIC);

        Info info = currentMarker.mark(frame);
        Mat outFrame = info.getImage();
        int objectCount = info.getCount();
        Image imageToShow = Utils.mat2Image(outFrame);

        updateImageView(currentFrame, imageToShow);

        String fps = DF.format(capture.get(Videoio.CAP_PROP_FPS));
        //udpate the text
        updateLabel("# " + objectCount + " @ " + fps + " FPS");
    }


    /**
     * Update the {@link ImageView} in the JavaFX main thread
     *
     * @param view  the {@link ImageView} to update
     * @param image the {@link Image} to show
     */
    private void updateImageView(ImageView view, Image image) {
        Utils.onFXThread(view.imageProperty(), image);
    }

    /**
     * The action triggered by pushing the button on the GUI
     * @param event the push button event
     */
    @FXML
    protected void startCamera(ActionEvent event) {
        if (!this.cameraActive) {
            // start the video capture
            this.capture.open(cameraId);

            // is the video stream available?
            if (this.capture.isOpened()) {
                this.cameraActive = true;

                // grab a frame every 33 ms (30 frames/sec)
                Runnable frameGrabber = new Runnable() {
                    Mat frame = null;

                    @Override
                    public void run() {
                        // effectively grab and process a single frame
                        frame = grabFrame();
                        processFrame(frame);
                    }
                };

                this.timer = Executors.newSingleThreadScheduledExecutor();
                this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);

                // update the button content
                this.button.setText("Stop Camera");
            } else {
                // log the error
                System.err.println("Impossible to open the camera connection...");
            }
        } else {
            // the camera is not active at this point
            this.cameraActive = false;
            // update again the button content
            this.button.setText("Start Camera");

            // stop the timer
            this.stopAcquisition();
        }
    }

    private void updateStatus(final String text) {
        if (text != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    status.setText(text);
                }
            });
        }
    }

    private void updateLabel(final String newText) {
        if (newText != null && !newText.equals(oldText)) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    oldText = newText;
                    text.setText(newText);
                }
            });
        }
    }

    /**
     * Get a frame from the opened video stream (if any)
     *
     * @return the {@link Mat} to show
     */
    private Mat grabFrame() {
        // init everything
        Mat frame = new Mat();

        // check if the capture is open
        if (this.capture.isOpened()) {
            try {
                // read the current frame
                this.capture.read(frame);

                // resize if set
                if (doResize) {
                    Imgproc.resize(frame, frame, new  Size(640, 360), 0, 0, Imgproc.INTER_CUBIC);
                }

                // if the frame is not empty, process it
                if (!frame.empty()) {
                    //TODO use this to filter black an white instead of colors...
                    //Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2GRAY);
                }

            } catch (Exception e) {
                // log the error
                System.err.println("Exception during the image elaboration: " + e);
            }
        }

        return frame;
    }

    /**
     * Stop the acquisition from the camera and release all the resources
     */
    private void stopAcquisition() {
        if (this.timer != null && !this.timer.isShutdown()) {
            try {
                // stop the timer
                this.timer.shutdown();
                this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                // log any exception
                System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
            }
        }

        if (this.capture.isOpened()) {
            // release the camera
            this.capture.release();
        }
    }

    /**
     * On application close, stop the acquisition from the camera
     */
    protected void setClosed() {
        this.stopAcquisition();
    }

}