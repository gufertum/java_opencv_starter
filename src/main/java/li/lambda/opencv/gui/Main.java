package li.lambda.opencv.gui;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.opencv.core.Core;

import java.net.URL;

/**
 * The main class for a JavaFX application. It creates and handle the main
 * window with its resources (style, graphics, etc.).
 * Taken from:
 * <a href="mailto:luigi.derussis@polito.it">Luigi De Russis</a>
 * and enhanced.
 */
public class Main extends Application {

    private FXMLLoader loader = null;

    @Override
    public void start(Stage primaryStage) {
        try {
            URL fxml = getClass().getResource("/fxml/Main.fxml");
            //System.out.println("URL = " + fxml.toExternalForm());
            // load the FXML resource
            loader = new FXMLLoader(fxml);
            // store the root element so that the controllers can use it
            BorderPane rootElement = (BorderPane) loader.load();
            // create and style a scene
            Scene scene = new Scene(rootElement, 720, 480);
            scene.getStylesheets().add(getClass().getResource("/fxml/application.css").toExternalForm());
            // create the stage with the given title and the previously created scene
            primaryStage.setTitle("OpenCV Starter");
            primaryStage.setScene(scene);
            // show the GUI
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop(){
        System.out.println("Good Bye.");
        if (loader != null) {
            MainController controller = loader.getController();
            controller.setClosed();
        }
    }

    /**
     * For launching the application...
     *
     * @param args optional params
     */
    public static void main(String[] args) {
        //load libs
        nu.pattern.OpenCV.loadShared();
        // load the native OpenCV library
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        // launch the app
        launch(args);
    }

}